<?php 

include_once('functions/connection.php');
include_once('functions/member.php');
include_once('functions/main.php');

$main = new main((new dbObj())->getConnstring());
$conn = (new dbObj())->getConnstring();
$member = new member($conn);

$params['limit'] = 20;
foreach ($_GET as $key => $value) {
    if(is_array($value)){
        foreach ($value as $key1 => $value1) {
            $params[$key][$key1] = htmlspecialchars(mysqli_escape_string($conn, $value1));
        }
    } else {
        $params[$key] = htmlspecialchars(mysqli_escape_string($conn, $value));
    }
}

if (empty($_GET)) {
    $params['link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?limit=20";
} else {
    $params['link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

$serverResponce = "";
if(!empty($_POST['addUser'])) {
    $error = htmlspecialchars($member->addMember($_POST));
    if($error == false)
        $serverResponce = "<script>$(document).ready(function () { swal('Successful entry!','You have added a member to the database!','success');});</script>";
    else
        $serverResponce = "<script>$(document).ready(function () { swal('Error!',\"$error\",'error');});</script>";
} else if(!empty($_POST['editUser'])) {
    $error = htmlspecialchars($member->editMember($_POST));
    if($error == false)
        $serverResponce = "<script>$(document).ready(function () { swal('Successful entry!','You have edited the details of a member','success');});</script>";
    else
        $serverResponce = "<script>$(document).ready(function () { swal('Error!',\"$error\",'error');});</script>";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Test Dashboard</title>

    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>

    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>


    <div class="content-main  container">

        <nav class="pcoded-navbar menupos-fixed menu-light ">
            <div class="navbar-wrapper content-main  container">
                <div class="navbar-content scroll-div ">
                    <ul class="nav pcoded-inner-navbar ">
                        <li class="nav-item pcoded-menu-caption">
                            <label>Dashboard</label>
                        </li>
                        <li class="nav-item"><a href="members.php" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-server"></i></span><span class="pcoded-mtext">Members</span></a></li>
                        <li class="nav-item">
                            <a href="send.php" class="nav-link ">
                                <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span><span
                                    class="pcoded-mtext">Send SMS</span></a>

                        </li>
                        <li class="nav-item"><a href="index.php" class="nav-link "><span class="pcoded-micon"><i
                                        class="feather icon-activity"></i></span><span
                                    class="pcoded-mtext">SMS history</span></a></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="pcoded-main-container">
            <div class="pcoded-content">









<div class="row">

    <div class="col-sm-12">
        <div class="tab-content">

            <div class="modal fade add-members" role="dialog" aria-labelledby="myLargeModalLabel3" aria-hidden="true" id="add_member_modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">ADD members</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body  p-5">
                            <form id="addmemberForm" class="memberForm" method="POST" novalidate="novalidate">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Name</label>
                                            <input type="text" class="form-control" name="Name" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Telephone</label>
                                            <input type="text" name="Telephone" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer  p-4">
                                <button type="submit" class="btn btn-rounded btn-success" name="addUser" value="add">Save</button>
                                <button type="button" data-dismiss="modal" class="btn btn-rounded  btn-secondary">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade edit-member" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" id="edit_member_modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                            <h4 class="modal-title text-uppercase font-weight-bold">edit member</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                    <form id="editmemberForm" method="POST" novalidate="novalidate">
                        <div class="modal-body  p-5">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">ID</label>
                                            <input type="number" readonly id="edit_ID" name="ID" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Name</label>
                                            <input type="text" class="form-control" name="Name" placeholder="Name" id="edit_Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Telephone</label>
                                            <input type="text" class="form-control" id="edit_Telephone" name="Telephone" placeholder="Phone: 0911001122" required>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer  p-4">
                            <button type="submit" class="btn btn-rounded btn-success" name="editUser" value="edit">Save</button>
                            <button type="button" data-dismiss="modal" class="btn btn-rounded  btn-secondary">Cancel</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form method="get">
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <h5 class="card-title float-left align-self-center text-uppercase">members</h5>
                            </div>
                            <div  class="col-sm-5 col-md-5 col-lg-5">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-right d-none d-xl-inline-block d-lg-inline-block d-md-inline-block">
                                            <div class="search"> <span class="fa fa-search"></span>
                                                <input placeholder="Search.." name="q" value="<?php if(!empty($_GET['q'])) echo($_GET['q']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 mb-4">
                                <a data-toggle="modal" href="#" data-target=".add-members" class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Add member</a>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table">
                            <?php 
                                $params['manage_member'] = true; 
                                $member->get_members($params);
                                $params["total"] = $member->total;
                            ?>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6 page-n">Show: 
                            <a class="<?php if($params['limit']==10) echo 'active'; ?>" href="<?php echo($params['link'].'&limit=10');?>">10</a> 
                            <a class="<?php if($params['limit']==20) echo 'active'; ?>" href="<?php echo($params['link'].'&limit=20');?>">20</a> 
                            <a class="<?php if($params['limit']==50) echo 'active'; ?>" href="<?php echo($params['link'].'&limit=50');?>">50</a>
                            of <?php echo $params["total"]; ?> results
                        </div>
    
                        <div class="container w-50">                        
                            <ul class="pagination">
                                <?php
                                    $main->createPagination($params);
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>










            </div>
        </div>

    </div>
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->


    <script src="assets/js/vendor-all.min.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>

    <script src="assets/js/plugins/jquery.validate.min.js"></script>

    <script src="assets/js/plugins/sweetalert.min.js"></script>
    <script>
        function prepare_edit(element){
            var data_row = $(element).parent().parent().parent();

            $('#edit_ID').val(data_row.children('input.ID').val());
            $('#edit_Name').val(data_row.children('input.Name').val());
            $('#edit_Telephone').val(data_row.children('input.Telephone').val());
        }

        function deleteMember(name,ID) {
            var conf = confirm('Delete ' + name + '?');
            if (conf) {
                var dataString = 'action=deleteMember&ID=' + ID;
                $.ajax
                ({
                    type: "POST",
                    url: "functions/PHPconnect.php",
                    data: dataString,
                    success: function (data)
                    {
                        console.log(data);
                        if (data.startsWith('Error')) {
                            alert('error deleting record check log for details');
                        } else {
                            location.reload();
                        }
                    }
                });
            }
        }
    </script>
    <?php echo $serverResponce; ?>
</body>
</html>