<?php 

include_once('functions/connection.php');
include_once('functions/member.php');
include_once('functions/main.php');

$main = new main((new dbObj())->getConnstring());
$conn = (new dbObj())->getConnstring();
$member = new member($conn);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Test Dashboard</title>
    <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <div class="content-main  container">
        <nav class="pcoded-navbar menupos-fixed menu-light ">
            <div class="navbar-wrapper content-main  container">
                <div class="navbar-content scroll-div ">
                    <ul class="nav pcoded-inner-navbar ">
                        <li class="nav-item pcoded-menu-caption">
                            <label>Dashboard</label>
                        </li>
                        <li class="nav-item"><a href="members.php" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-server"></i></span><span class="pcoded-mtext">Members</span></a></li>
                        <li class="nav-item">
                            <a href="send.php" class="nav-link ">
                                <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span><span
                                    class="pcoded-mtext">Send SMS</span></a>

                        </li>
                        <li class="nav-item"><a href="index.php" class="nav-link "><span class="pcoded-micon"><i
                                        class="feather icon-activity"></i></span><span
                                    class="pcoded-mtext">SMS history</span></a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="pcoded-main-container">
            <div class="pcoded-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-3 col-lg-3">
                                            <h5 class="card-title float-left align-self-center text-uppercase">member</h5>
                                        </div>
                                        <div  class="col-sm-5 col-md-5 col-lg-5">
                                            <div class="float-right d-none d-xl-inline-block d-lg-inline-block d-md-inline-block">
                                                <div class="search"> <span class="fa fa-search"></span>
                                                    <input placeholder="Search.." id="search_q" name="q" value="<?php if(!empty($_GET['q'])) echo($_GET['q']); ?>">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="table-responsive">
                                        <div class="d-flex justify-content-center">
                                            <div class="spinner-border" role="status" id="loading_members" style="display: none;">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                        <table style="height: 400px;" class="table color-table primary-table table-responsive table-sm" id="MemberTable">
                                            <?php 
                                                $params = array("group_phones"=>true,"limit"=>"all");
                                                $member->get_members($params);
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <h5>Message</h5>
                        <textarea class="form-control max-textarea" maxlength="255" rows="4" data-threshold="1000" id="message"></textarea>
                        <button class="btn  btn-primary" type="submit" onclick="sendSMS();">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->
    <script src="assets/js/vendor-all.min.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/plugins/sweetalert.min.js"></script>
    <script>
    var numbers = [];
    $(document).ready(function()
    {
        $("#search_q").on('keyup', function(e) {
            if(e.keyCode == 13)
            {
                search_members();
            }
        });
        $('#loading_members').css("display","inline-block");
        var rows = $('#MemberTable tbody tr');
        for(var i = 0; i < rows.length; i++) {
            $(rows[i]).addClass("table-primary");
            numbers.push($(rows[i]).children('td.Telephone').text());
        }

        rows.click(function(event) {
            if($(this).hasClass("table-primary")){
                $(this).removeClass("table-primary");
                var index = numbers.indexOf($(this).children('td.Telephone').text());
                if(index != -1)
                    numbers.splice(index, 1);
            } else {
                $(this).addClass("table-primary");      
                numbers.push($(this).children('td.Telephone').text());
            }
        });
        $('#loading_members').css("display","none");
    });

    function search_members() {
        $('#loading_members').css("display","inline-block");
        var searchData = {data:'Member',
                          limit:'all',
                          q:$('#search_q').val()
                      };
        $.ajax
        ({
            type: "GET",
            url: "functions/PHPconnect.php",
            data: searchData,
            success: function (html)
            {
                $("#MemberTable").html(html);
                numbers = [];
                var rows = $('#MemberTable tbody tr');
                for(var i = 0; i < rows.length; i++) {
                    $(rows[i]).addClass("table-primary");
                    numbers.push($(rows[i]).children('td.Telephone').text());
                }

                rows.click(function(event) {
                    if($(this).hasClass("table-primary")){
                        $(this).removeClass("table-primary");
                        var index = numbers.indexOf($(this).children('td.Telephone').text());
                        if(index != -1)
                            numbers.splice(index, 1);
                    } else {
                        $(this).addClass("table-primary");      
                        numbers.push($(this).children('td.Telephone').text());
                    }
                });
                $('#loading_members').css("display","none");
            }
        });
    }

    function sendSMS(){ // element is either weather-sms page or market-sms page
        var message = $('#message').val();  // get message in editable message box
        if(message != null && message != ''){
            var phones = [];
            for (var i = numbers.length - 1; i >= 0; i--) {    // create array of phones and their assosiated message
                    phones.push(numbers[i]);
            }
            if(phones.length < 1) {
                swal('Error!','No numbers selected','error');
            } else {
                var toSend = {"Message":message, "Phones":phones};
                
                var dataString = 'jsonData='+JSON.stringify(toSend);
                $.ajax
                ({
                    type: "POST",
                    url: "sms/print.php",
                    data: dataString,
                    cache: false,
                    success: function(html)
                    {
                        if(html.startsWith('Error'))
                            swal('Error!',html,'error');
                        else
                            swal('Sent!',html,'success');
                    } 
                });
            }
        } else {
            swal('Error!',"No message to send",'error');
        }
    }
    </script>

</body>
<!-- Mirrored from html.phoenixcoded.net/mintone/bootstrap/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 10:47:26 GMT -->

</html>