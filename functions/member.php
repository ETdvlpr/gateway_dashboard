<?php
/**
 * Has method that returns a table with all members data
 * in display HTML format given a certain set of filtering parameters.
 */
class member {

    /**
     * The database connection object to be 
     * initialized in the constructor
     *
     * @var conn
     * @access private
     */
    private $conn;

    /**
     * Total results found in query
     * Mainly used for creating pagination
     *
     * @var total
     * @access public
     */
    public $total;

    /**
     * Initializes the database connection object used in this class
     *
     * @param object $connString Database connection object instance 
     */
    function __construct($connString) {
        $this->conn = $connString;
    }

    /**
     * Closes database connection link object
     */
    function __destruct() {
        $this->conn->close();
    }


    public function get_members($params) {

        $filter = 'where 1=1';

        if (isset($params['q']) && $params['q'] != '') {

            $qs = array_filter(explode("+", $params['q']));
            if (!empty($qs)) {
                $filter .= " AND ( (1 != 1) ";
                foreach ($qs as $value) {
                    $q = array_filter(explode(" ", $value));
                    if (!empty($q)) {
                        $filter .= " OR ";
                        $filter .= "((1=1)";
                        foreach ($q as $query) {
                            $filter .= " AND (Name LIKE '%$query%'OR Telephone LIKE '%$query%')";
                        }
                        $filter .= ")";
                    }
                }
                $filter .= ")";
            }
        }

        if(!empty($params['group_phones'])) {
            $filter .= " AND Telephone IS NOT NULL";
        }

        if (isset($params['limit']) && !is_numeric($params['limit']))
            $limit = '';
        else
            $limit = isset($params['limit']) ? (isset($params['offset']) && is_numeric($params['offset']) ? "limit " . floor($params['offset']) . ", " . floor($params['limit']) : "limit " . floor($params['limit'])) : "limit 25";

        $sql = "SELECT *
        FROM member $filter ORDER BY Name $limit";

        $sql_count = "SELECT count(*) as count
        FROM member";

        $rsData = $this->conn->query($sql);
        if ($rsData->num_rows > 0) {
            $rsCount = $this->conn->query($sql_count);
            $count = $rsCount->fetch_array();
            $this->total = $count['count'];
            echo "<thead>\n";
            echo '<tr class="table_header"' . ">\n";
            echo '<th scope="row">#</th> <th>Name</th> <th>Telephone</th>' . "\n";
            if(!empty($params['manage_member'])) echo "<th></th>";
            echo '</tr></thead>' . "\n";
            echo '<tbody >' . "\n";
            $i = 1;
            while ($row = $rsData->fetch_array()) {

                echo "<tr>\n";

                if(!empty($params['manage_member'])) {
                    echo "<input type=\"hidden\" class=\"ID\" value=\"".$row["ID"]."\">";
                    echo "<input type=\"hidden\" class=\"Name\" value=\"".$row["Name"]."\">";
                    echo "<input type=\"hidden\" class=\"Telephone\" value=\"".$row["Telephone"]."\">";
                }
                
                echo "<th scope=\"row\">" . $i++ . "</th> \n";

                echo "<td class=\"Name\">$row[Name]</td>\n";

                echo "<td class=\"Telephone\">$row[Telephone]</td>\n";
                
                if(!empty($params['manage_member'])) 
                    echo "<td class='text-light-blue'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-v'></i></a>
                        <div class='dropdown-menu'> 
                        <a class='dropdown-item' data-toggle='modal' href='#' data-target='.edit-member' onClick='prepare_edit(this);'>Edit</a> 
                        <a class='dropdown-item' href='#'>Send Message</a> 
                        <a class='dropdown-item text-light-danger' href='' onClick=\"deleteMember('$row[Name]',$row[ID]);\">Delete</a> </div>
                        </td>";

                echo "</tr>\n";
            }
            echo "</tbody>\n";
        } else {
            echo '<h2 class="text-center"> No data available for your search </h2>';
        }
    }

    /**
     * Adds a member to the database
     *
     * @param    details of the member to be entered
     * @return   error
     */
    public function addMember($member) {
        // prepare and bind
        $stmt = $this->conn->prepare("INSERT INTO `member` ( `Name`, `Telephone`) VALUES (?, ?)");
        echo $this->conn->error;
        $stmt->bind_param("ss", $member['Name'], $member['Telephone']);
        $stmt->execute();
        $error = $stmt->error;
        $stmt->close();
        return $error;
    }

    /**
     * Update attributes of member
     *
     * @param    details of the member to be entered
     * @return   error
     */
    public function editMember($Member) {
        // prepare and bind
        $stmt = $this->conn->prepare("UPDATE `member` SET `Name` = ?, `Telephone` = ? WHERE `ID` = ?");
        $stmt->bind_param("ssi", $Member['Name'], $Member['Telephone'], $Member['ID']);
        $stmt->execute();
        $error = $stmt->error;
        $stmt->close();
        return $error;
    }

    /**
     * Delete a Member from the database
     *
     * @param    ID of the member to be deleted
     * @return   error
     */
    public function deleteMember($Member) {
        // prepare and bind
        $stmt = $this->conn->prepare("DELETE FROM `member` WHERE `ID` = ?");
        $stmt->bind_param("i", $Member['ID']);
        $stmt->execute();
        $error = $stmt->error;
        $stmt->close();
        return $error;
    }
}

?>