<?php
/**
 * Has method that returns a table with all SMS history data
 * in display HTML format given a certain set of filtering parameters.
 */
class SMShistory {

    /**
     * The database connection object to be 
     * initialized in the constructor
     *
     * @var conn
     * @access private
     */
    private $conn;

    /**
     * Total results found in query
     * Mainly used for creating pagination
     *
     * @var total
     * @access public
     */
    public $total;

    /**
     * Initializes the database connection object used in this class
     *
     * @param object $connString Database connection object instance 
     */
    function __construct($connString) {
        $this->conn = $connString;
    }

    /**
     * Closes database connection link object
     */
    function __destruct() {
        $this->conn->close();
    }

    /**
     * Echos a table containing sms history data from the database
     * Filters the data based on selected parameters
     * Sets $total with count of total results for parameters
     *
     * @param    array   $params offset, limit, q {search query}.
     * @return   string  $html   table of results with sms history details
     *                           or no data available if parameters yield no results.
     */
    public function get_SMShistory($params) {

        $filter = 'where 1=1';

        $limit = !empty($params['limit']) ? (!empty($params['offset']) && is_numeric($params['offset']) ? "LIMIT " . floor($params['offset']) . ", " . floor($params['limit']) : "limit " . floor($params['limit'])) : "LIMIT 50";

        $sql = "SELECT * FROM `Message` ORDER BY `Message`.`send_time` DESC $limit;";
        $rsData = $this->conn->query($sql);
        if ($rsData->num_rows > 0) {
            $rsCount = $this->conn->query("SELECT count(*) as count FROM Message`;");
            $count = $rsCount->fetch_array();
            $this->total = $count['count'];

            $rowMsg = $rsData->fetch_assoc();
            $day = explode(" ", $rowMsg['send_time']);
            echo "
                <div class='text-light-blue mb-1'>".date('D, d.m.Y',strtotime($day[0]))."</div>
                <table class='table table-hover primary-table'>
                    <tbody>";
                    echo "<tr>
                            <td></td>
                            <td class='font-weight-bold'>$rowMsg[Count] Recepients</td>
                            <td class='line-h max-lines'>".str_replace("\n", "<br>", $rowMsg['Message'])."</td>
                            <td>$day[1]</td>
                        </tr>";
                while ($rowMsg = $rsData->fetch_assoc()) {
                    if($day[0] == explode(" ", $rowMsg['send_time'])[0]) {
                        $day = explode(" ", $rowMsg['send_time']);
                        echo "  
                        <tr>
                            <td></td>
                            <td class='font-weight-bold'>$rowMsg[Count] Recepients</td>
                            <td class='line-h max-lines'>".str_replace("\n", "<br>", $rowMsg['Message'])."</td>
                            <td>$day[1]</td>
                        </tr>";
                    } else {
                        $day = explode(" ", $rowMsg['send_time']);
                        echo "
                    </tbody>
                </table>
                <div class='clearfix'></div>
                <div class='text-light-blue mb-1'> " . date('D, d.m.Y',strtotime($day[0])) . " </div>
                <div class='clearfix'></div>
                <table class='table table-hover primary-table'>
                    <tbody>
                        <tr>
                            <td></td>
                            <td class='font-weight-bold'>$rowMsg[Count] Recepients</td>
                            <td class='line-h max-lines'>".str_replace("\n", "<br>", $rowMsg['Message'])."</td>
                            <td>$day[1]</td>
                        </tr>";
                    }
                }
                echo "
                    </tbody>
                </table>
            </div>";
        } else {
            echo '<h3 class="text-center"> No messages found </h3>';
        }
    }
}

?>