<?php
/**
 * Main class with essential methods. These methods include
 * counter values for multiple tables and various option lists
 * to be used in most of the pages through the site
 */
class main {

    /**
     * The database connection object to be 
     * initialized in the constructor
     *
     * @var conn
     * @access private
     */
    private $conn;

    /**
     * Initializes the database connection object used in all methods of this class
     *
     * @param object $connString Database connection object instance 
     */
    function __construct($connString) {
        $this->conn = $connString;
    }

    /**
     * Closes database connection link object
     */
    function __destruct() {
        $this->conn->close();
    }

    /**
     * outputs total amount of registered members
     *
     * @return      int amount of registered members
     */
    public function get_memberCount() {
        $rsUserCount = $this->conn->query("SELECT count(*) AS COUNT FROM member;");
        $count = $rsUserCount->fetch_array();
        echo $count['COUNT'];
    }

    /**
     * outputs total amount of sent SMS
     *
     * @return      int amount of SMSlog entries
     */
    public function get_SMSCount() {
        $rsSMScount = $this->conn->query("SELECT count(*) AS COUNT FROM SMSlog;");
        $count = $rsSMScount->fetch_array();
        echo $count['COUNT'];
    }








    /**
     * Echos pagination links for a table.
     * 
     * The table generating functions are written in such a way that they accept
     * a certain offset and data limit.
     * Given the total amount of data and current offset this function returns 
     * a set of appropriate pagination links.
     *
     * @param    array   $params link -> {The current uri of the search page}, total -> {The total amount of results found}, offset, limit
     * @return   string   Pagination links, Maximum of 5 pages with previous and next links
     */
    function createPagination($params) {
        $curPage = isset($params['offset']) ? $params['offset'] / $params['limit'] : 0;
        $start = isset($params['offset']) ? floor($curPage / 5) * 5 : 0;
        $size = isset($params['limit']) && is_numeric($params['limit']) ? $params['limit'] : $params['total'];
        $link = $params['link'];
        $total = $params['total'];
        
        //remove offset from link
        $pos = stripos($link, "&offset=");
        if($pos != false){
                $pos2 = stripos(substr($link, $pos+1), "&");
                $link = ($pos2 == false) ? substr($link, 0, $pos) : substr($link, 0, $pos).substr($link, ($pos + $pos2 + 1));
        }
        
        if ($total == 0 && $size == 0)
            $total = $size = 1;

        if ($start == 0) {
            echo '<span class="page-item disabled"><span class="page-link">&laquo;</span></span>' . "\n";
            echo '<span class="page-item disabled"><span class="page-link"><</span></span>' . "\n";
        } else {
            echo "<a href=\"$link\" data-toggle=\"tooltip\" title=\"Go to first item\" class=\"page-item\"><span class=\"page-link\">&laquo;</span></a>\n";
            echo "<a href=\"$link&offset=" . (($start - 1) * $size) . "\" class=\"page-item\" data-toggle=\"tooltip\" title=\"Previous\"><span class=\"page-link\"><</span></a>\n";
        }

        for ($i = 0; $i <= 4 && $start + $i < $total / $size; $i++) {
            if ($start + $i == $curPage)
                echo "<a href=\"$link&offset=" . ($curPage * $size) . "\" class=\"page-item active\"><span class=\"page-link\">" . ($curPage + 1) . "</span></a>\n";
            else
                echo "<a href=\"$link&offset=" . (($start + $i) * $size) . "\" class=\"page-item\"><span class=\"page-link\">" . ($start + $i + 1) . "</span></a>\n";
        }
        if ($start + 5 < $total / $size) {
            echo "<a href=\"$link&offset=" . (($start + 5) * $size) . "\" class=\"page-item\" data-toggle=\"tooltip\" title=\"Next\"><span class=\"page-link\">></span></a>\n";
            echo "<a href=\"$link&offset=" . ($total - ($total % $size)) . "\" class=\"page-item\"><span class=\"page-link\" data-toggle=\"tooltip\" title=\"Go to last item\" >&raquo;</span></a>\n";
        }
    }

}

?>