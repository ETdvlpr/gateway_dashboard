<?php
/**
 * Connection object to initialize database connection
 */
Class dbObj {

    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "test_dashboard";

    /**
     * instantiates a connection object
     * 
     * @return   object  $con   instance of a database connection object.
     */
    function getConnstring() {
        $con = mysqli_connect($this->servername, $this->username, $this->password, $this->dbname) or die("Connection failed: " . mysqli_connect_error());

        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        return $con;
    }

}

?>