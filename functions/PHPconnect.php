<?php
/**
 * list of includes for classes with required methods
 */
include_once('main.php');
include_once('member.php');
include_once('connection.php');

// if (session_status() == PHP_SESSION_NONE) {
//     session_start();
// }
// if (!isset($_SESSION['logged_in']))
//     exit();

$db = new dbObj();
$main = new main($db->getConnstring());
$member = new member($db->getConnstring());


// sanitize input
$conn = $db->getConnstring();
$params = array();
foreach ($_REQUEST as $key => $value) {
    if(is_array($value)){
        foreach ($value as $key1 => $value1) {
            $params[$key][$key1] = htmlspecialchars(mysqli_escape_string($conn, $value1));
        }
    } else {
        $params[$key] = htmlspecialchars(mysqli_escape_string($conn, $value));
    }
}
// $params['UserID'] = $_SESSION['ID'];

if (isset($params['data'])) {
    $data = $params['data'];
    
    if($data == "Member") {
        $member->get_members($params);
    } else {
        echo "error : requested data not available";
    }
} else if (isset($params['action'])) {
    $action = $params['action'];

    if ($action == 'deleteMember') {
        $member->deleteMember($params);
    } else {
        echo "error : requested action not implemented";
    }
} else {
    echo "error : no data requested, no action specifed";
}
?>