<?php 

include_once('functions/connection.php');
include_once('functions/SMShistory.php');
include_once('functions/main.php');

$conn = (new dbObj())->getConnstring();
$main = new main((new dbObj())->getConnstring());
$SMShistory = new SMShistory((new dbObj())->getConnstring());

$params = array("limit"=>50);
foreach ($_GET as $key => $value) {
    $params[$key] = $value;
}
if (empty($_GET)) {
    $params['link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?limit=50";
} else {
    $params['link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Test Dashboard</title>
    <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        .max-lines {
          display: block;/* or inline-block */
          text-overflow: ellipsis;
          word-wrap: break-word;
          overflow: hidden;
          max-height: 6.6em;
          line-height: 1.8em;
        }
    </style>
</head>

<body>
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>

    <div class="content-main  container">

        <nav class="pcoded-navbar menupos-fixed menu-light ">
            <div class="navbar-wrapper content-main  container">
                <div class="navbar-content scroll-div ">
                    <ul class="nav pcoded-inner-navbar ">
                        <li class="nav-item pcoded-menu-caption">
                            <label>Dashboard</label>
                        </li>
                        <li class="nav-item"><a href="members.php" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-server"></i></span><span class="pcoded-mtext">Members</span></a></li>
                        <li class="nav-item">
                            <a href="send.php" class="nav-link ">
                                <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span><span
                                    class="pcoded-mtext">Send SMS</span></a>

                        </li>
                        <li class="nav-item"><a href="index.php" class="nav-link "><span class="pcoded-micon"><i
                                        class="feather icon-activity"></i></span><span
                                    class="pcoded-mtext">SMS history</span></a></li>
                    </ul>
                </div>
            </div>
        </nav>





















                <div class="row">
<div class="col-lg-12 col-md-12">
    <div class="card">
        <div class="card-body position-relative">
            <h5 class="card-title float-left  align-self-center text-uppercase">Messages Sent</h5>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="scrollbox row">
                <div class="col-lg-12" id="history_view" style="height:370px;">
                    <div class="table-responsive">
                        <?php 
                            $SMShistory->get_SMShistory($params);
                            $params["total"] = $SMShistory->total; 
                        ?>
                </div>
                <div class="clearfix"></div>
                        <?php
                            $main->createPagination($params);
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>
                </div>












            </div>
        </div>
    </div>
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->
    <script src="assets/js/vendor-all.min.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/plugins/sweetalert.min.js"></script>
    <script>
        $(document).ready(function()
        {
            var px=new PerfectScrollbar('#history_view',{wheelSpeed:.5,swipeEasing:0,wheelPropagation:1,minScrollbarLength:40,});
        });
    </script>
</body>
<!-- Mirrored from html.phoenixcoded.net/mintone/bootstrap/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 10:47:26 GMT -->

</html>